import wx

def menu():
    app=wx.App()
    frame=wx.Frame(None,-1,'myIco')
    frame.SetToolTip(wx.ToolTip('this is a frame'))
    frame.SetSize(wx.Size(300,500))
    frame.SetPosition(wx.Point(200,200))
    frame.SetIcon(wx.Icon('smile.ico', wx.BITMAP_TYPE_ICO))
    frame.Center()
    menubar=wx.MenuBar()
    file=wx.Menu()
    edit=wx.Menu()
    help=wx.Menu()

    file.Append(101, '&Open', 'Open a new document')
    file.Append(102, '&Save', 'Save the document')
    file.AppendSeparator()
    quit = wx.MenuItem(file, 105, '&Quit\tCtrl+Q', 'Quit the Application')
    file.AppendItem(quit)


    edit.Append(201, 'check item1', '', wx.ITEM_CHECK)
    edit.Append(202, 'check item2', '', kind=wx.ITEM_CHECK)


    menubar.Append(file, '&File')
    menubar.Append(edit, '&Edit')
    menubar.Append(help, '&Help')




    frame.SetMenuBar(menubar)

    frame.CreateStatusBar()
    frame.Show()
    app.MainLoop()


if __name__ == '__main__':
    menu()

